# Media Organizer

This project is meant to scan a directory containing photos and extracts metadata such as capture date and hash. This information is then inserted into a sqlite table along with the path to the photo.

To create blank table run:
`reset_tables`

To scan a directory run:
`scan_media <directory>`
