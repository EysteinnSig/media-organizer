FROM python:3.7.14-alpine3.16

MAINTAINER eysteinnsig@gmail.com

ENV DEBIAN_FRONTEND=noninteractive

RUN apk update && apk upgrade

RUN apk --update add libxml2-dev libxslt-dev libffi-dev gcc musl-dev libgcc openssl-dev curl
RUN apk add jpeg-dev zlib-dev freetype-dev lcms2-dev openjpeg-dev tiff-dev tk-dev tcl-dev

RUN pip3 install Pillow

RUN apk add sqlite

#COPY *.py /usr/local/bin/
#COPY create_table /usr/local/bin

COPY bin/* /usr/local/bin/
#RUN apt update 
#&& apt install -y \
#    zlib1g \
#    zlib1g-dev    

#RUN python3 -m pip install --upgrade Pillow
 
