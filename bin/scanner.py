#!/usr/bin/env python3

import argparse

import sqlite3
from PIL import Image
from pathlib import Path
from datetime import datetime
import hashlib

def insert_db(dbfile, hashid, unixtime, path):
    con = sqlite3.connect(dbfile)
    cur = con.cursor()
    cmd = "INSERT OR REPLACE INTO photos (hash, unixtime, path) VALUES ('{hashid}',{unixtime},'{path}');".format(hashid=hashid, unixtime=unixtime, path=str(path))

    try:
        count = cur.execute(cmd)
        con.commit()
    except sqlite3.IntegrityError as e:
        print('Error: {path}'.format(path=path))

    cur.close()
    con.close()


def get_date_taken(path):
    try: 
        dtstr = Image.open(path)._getexif()[36867]
        return datetime.strptime(dtstr, '%Y:%m:%d %H:%M:%S')
    except:
        return None

def hash_file(path):
    # Python program to find SHA256 hash string of a file
    sha256_hash = hashlib.sha256()
    with open(path,"rb") as f:
        # Read and update hash string value in blocks of 4K
        for byte_block in iter(lambda: f.read(4096),b""):
            sha256_hash.update(byte_block)
        return sha256_hash.hexdigest()


def parse_args():
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser()
    script_dir = Path(__file__).resolve().parent

    parser.add_argument('-R','--recursive', action='store_true', help='Scan recursively into subfolders.')
    parser.add_argument('-d','--dbfile', help='Path to db file.')
    parser.add_argument('paths', type=str, nargs='*', help='Path to scan')

    opts = parser.parse_args()
    params = vars(opts)
    return params

def scan_directory(dbpath, path, recursive=False):
    if recursive:
        globfunc = Path(path).rglob("*.jpg")
    else:
        globfunc = Path(path).glob("*.jpg")
    
    for p in globfunc:
        try:
            hash = hash_file(p)
            dt = get_date_taken(p)
            if dt:
                #print(hash, dt, p.resolve())
                insert_db(dbpath, hash, round(dt.timestamp()), p.resolve())
                print("Processed file: {path}".format(path=p.resolve()))
            else:
                print("Error getting time from: {path}".format(path=p.resolve()))

        except Exception as e:
            print("Error processing file: {path}".format(path=p.resolve()))
            print(str(e))

def main():
    args = parse_args()
    for path in args.get('paths'):
        scan_directory(args.get('dbfile'), path, args.get('recursive'))


if __name__ == "__main__":
    main()

